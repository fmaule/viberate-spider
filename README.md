# spotify-spider #

This script will assist you in getting (almost) free VIBerate tokens (www.viberate.io) without all the hassle of manually searching for artists.
# TODO #

- Give back some better formatted info
- Try to find automatically the social pages for the artist (FB, Soundcloud, youtube, ...) leveraging the related APIs, in order to speed up even more the addition of the Artist on Viberate
- Must search for the following social pages
    - spotify (requried)
    - facebook (required)
    and at least one of the following
    - twitter
    - soundcloud
    - youtube
    - songkick
    - instagram

# HOW-TO run #

Clone repo, npm install, and run the script with node:

```node index.js```

The script will get an auth token on Spotify (with the secrets in secrets/spotify.json) and then a prompt:

```viberate-spider>```

# Available commands #

There are two things you can do for the moment: check if a single artist in spotify is present on Viberate or lookup 10 related spotify artists are present.

To check for a single artist, just paste the Spotify Artist URL (in the artist page, click on the 3 dots, then Share, then Copy Spotify URI)

```viberate-spider> spotify:artist:1Zp054Jc86WVKCxKEqZGOA```

The output will be like this:

```
artist ID 1Zp054Jc86WVKCxKEqZGOA
looking up artist: Jack Garratt ...
{ viberate:
   [ { name: 'JACK GARRATT',
       url: 'https://www.viberate.com/artists/profile/jack-garratt',
       genre: 'POP',
       subgenre: '' } ],
  spotify:
   { name: 'Jack Garratt',
     url: 'https://open.spotify.com/artist/1Zp054Jc86WVKCxKEqZGOA',
     genre:
      [ 'deep indie r&b',
        'indie anthem-folk',
        'indie folk',
        'indie r&b',
        'indietronica',
        'pop',
        'vapor soul' ] } }
```

meaning the artist *is* present on Viberate. If the artist is not present you will get back *Artist not present!*

If you want to search for related artists, just prepend *related* to the artist URL, like this:

```viberate-spider> related spotify:artist:1Zp054Jc86WVKCxKEqZGOA```

The script will give you back the list of the related artists:

```
viberate-spider> related artists: [ 'Shy Girls',
  'Låpsley',
  'Raleigh Ritchie',
  'SOHN',
  'Oh Wonder',
  'Jarryd James',
  'James Blake',
  'HONNE',
  'The Japanese House',
  'Banks',
  'Meg Mac',
  'Chet Faker',
  'Vallis Alps',
  'Aquilo',
  'Gallant',
  'Sundara Karma',
  'Sampha',
  'NAO',
  'Thomston',
  'Tom Odell' ]
```

And then it will search for all those in the Viberate database, giving all the output info, and at the end a list of the artists that are *not* present and that you can add.

```
artist to add: [ 'Chet Faker' ]
```
# Spotify secrets configuration #

Example configuration for spotify secrets in ```secrets/spotify.json```:

```
{
    "clientId" : "xxxxx",
    "clientSecret" : "yyyy",
    "redirectUri" : "http://localhost:8888/callback"
}
```
  