const SpotifyWebApi = require('spotify-web-api-node');
const sa = require('superagent');
const readline = require('readline');
const YouTube = require('youtube-node');

const spotifyConf = require('./secrets/spotify.json');
const youtubeConf = require('./secrets/youtube.json');
const facebookConf = require('./secrets/facebook.json');

const VIBERATE_ARTIST_URL='https://www.viberate.com/artists/profile';
const VIBERATE_SEARCH_URL='https://www.viberate.com/home/GetSearchResults';

const spotifyApi = new SpotifyWebApi(spotifyConf);
const youtubeApi = new YouTube();

const sfURLToArtist = url => /spotify\:artist\:([\da-zA-Z]+$)/.exec(url)[1];

const filterChannels = items => items.filter(entry => 
    entry.id.kind === 'youtube#channel' ||
    entry.snippet.channelTitle.includes('VEVO')
);

const channelURL = item => ({ name: item.snippet.channelTitle, url: `https://www.youtube.com/channel/${item.snippet.channelId}` });

const processViberateArtists = artistData => {
  const processedData = artistData.map(
    (artist) => ({
      name: artist.Name,
      url: `${VIBERATE_ARTIST_URL}/${artist.ArtistFriendlyUrl}`,
      genre: artist.ArtistGenreName,
      subgenre: artist.ArtistFirstSubGenreName
    })
  );
  return processedData;
}

const dataDiff = (viberate, spotify) => {
  const result = {
    viberate: viberate,
    spotify: { 
      name: spotify.name,
      url: spotify.external_urls.spotify,
      genre: spotify.genres
    }
  }
  return result;
}

const getSpotifyToken = async() => {
  const res = await spotifyApi.clientCredentialsGrant();
  const spotifyToken = res.body['access_token'];
  spotifyApi.setAccessToken(spotifyToken);
  return spotifyToken;  
}

const setTokens = async () => {
  // set youtube key
  youtubeApi.setKey(youtubeConf.token);
  // refresh spotify token
  const spotifyToken = await getSpotifyToken();
  return {
    youtube: true,
    spotify: spotifyToken
  }
}

// better lookup function on viberate, using search
const vrLookupArtist = async (name) => {
  const res = await sa.get(`${VIBERATE_SEARCH_URL}/`)
                      .query({ searchString: name })
                      .query({ searchType: 'artists' })
                      .query({ page: 1 });  
  const artistData = res.body;
  if (artistData.length)
    return processViberateArtists(artistData);
  else
    return false;
}

const waitForInput = async () => {
  var rl = readline.createInterface(process.stdin, process.stdout);
  rl.setPrompt('viberate-spider> ');
  rl.prompt();
  rl.on('line', async function(l) {
  
    if (l.substr(0,8) === "related ") {
      const artistURL = l.substr(8,l.length - 8);
      getRelatedArtists(artistURL)
        .then(async function(res) {
          const artists = res.map(function (artist) {
            return artist.name;
          });

          console.log('related artists:', artists); 
          
          let artistsToAdd = [];

          for (let artist of artists) {
            console.log('searching for', artist, '...');
            const artistFound = await vrLookupArtist(artist);
            console.log(artistFound);
            if (!artistFound) artistsToAdd.push(artist);
          }
          console.log('artist to add:', artistsToAdd);

          try {
            const promises = artistsToAdd.map(artist => youtubeSearch(artist));
            const artistsInfo = await Promise.all(promises);
            console.log(artistsInfo);
          } catch (e) { console.log(e) }


          artistsToAdd.map(artist => youtubeSearch(artist));
        })
        .catch(function(e) { console.log(e) });
    }
   
    if (l.substr(0,8) === "spotify:") {
      const artistInfo = await getArtist(l);
      console.log('looking up artist:', artistInfo.name, '...');
      const processedData = await vrLookupArtist(artistInfo.name);
      if (processedData) {
        console.log(dataDiff(processedData, artistInfo));
      } else {
        console.log('Artist not present!');
      }
    }

    rl.prompt();
  }).on('close',function(){
    process.exit(0);
  });
}

const getRelatedArtists = async (artistURL) => {
  const artistId = sfURLToArtist(artistURL);
  if (!artistId) throw new Error('wrong spotify url');

  const res = await spotifyApi.getArtistRelatedArtists(artistId);
  const relatedArtists = res.body.artists;
  return relatedArtists;
}

const getArtist = async (artistURL) => {
  const artistId = sfURLToArtist(artistURL);
  console.log('artist ID', artistId);
  if (!artistId) throw new Error('wrong spotify url');

  const res = await spotifyApi.getArtist(artistId);
  const artistInfo = res.body;
  return artistInfo;
}

const youtubeSearcha = async (artist) => {
  youtubeApi.setKey(youtubeConf.token);
  youtubeApi.search(artist, 2, function(error, result) {
  if (error) {
    console.log(error);
  }
  else {
    //console.log(JSON.stringify(result, null, 2));
    const filtered = filterChannels(result.items);
    console.log('only channels:', filtered[0].snippet.channelTitle);
  }
});
}

const youtubeSearch = async (artist) => {
  return new Promise((resolve,reject) => {
    youtubeApi.search(artist, 2, (err, data) => {
      if (err) throw err;
      const channels = filterChannels(data.items);
      const urls = channels.map(chan => channelURL(chan));
      resolve(urls);
    })
  }).catch(err => { throw err });
}

(async () => {
  console.log('Authorizing APIs...')
  const tokens = await setTokens();
  console.log('Youtube:', tokens.youtube);
  console.log('Spotify:', tokens.spotify.substr(0,15), '...');
  await waitForInput();
  /*
  artistsToAdd = ['Chet Faker', 'jack garratt', 'David Guetta', 'Tiesto', 'Eric Morillo'];
  try {
    const promises = artistsToAdd.map(artist => youtubeSearch(artist));
    const results = await Promise.all(promises);
    console.log('results are', results);
  } catch (e) { console.log(e) }*/
})();


